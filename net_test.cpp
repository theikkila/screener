/*
* NET_TEST.CPP
* Test for controlling server
* Author: Teemu Heikkilä <teemu.heikkila@pistoke.org>
*
*/

#include "controller.h"
#include <iostream>

int main(){
	networking::server cserver(14000);
	networking::message ms;

	while(true){
		cserver.update();
		if(cserver.getCommand(ms)){
			std::cout << ms.x << "," << ms.y << std::endl;
		}
	}
}
