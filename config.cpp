/*
* CONFIG.CPP
* Implementation of config parser
* Author: Teemu Heikkilä <teemu.heikkila@pistoke.org>
*
*/

#include "config.h"


/* Config initializer
* Read and parsing of config file
*/
screener::config::config(const std::string &path){
	std::string line, key, value;
	// Tile structure
	int tid=0;
	std::string tpath="";
	int tx = 0;
	int ty = 0;
	int tile_id = 0;
	screener::tile t = {0, "", 0, 0};
	std::ifstream config_file(path.c_str(), std::ifstream::in);
	// Read lines
	while(config_file.good()){
		std::getline(config_file, line);
		if(line[0] == '#'){
			continue;
		}
		if(line == ""){
			continue;
		}
		
		std::size_t found;
		if(checkSub(line)){
			// Save tile
			if(tile_id >= 1){
				t.id = tid;
				t.path = tpath;
				t.x = tx;
				t.y = ty;
				tiles_.push_back(t);
			}
			tile_id++;
		}
		found = line.find("=");
		if(found != line.npos){
			key = line.substr(0, found-1);
			value = line.substr(found+1);
			// Bloat but working - strip key and value
			std::stringstream tmp;
			tmp << key;
			key.clear();
			tmp >> key;
			tmp.clear();
			tmp << value;
			value.clear();
			tmp >> value;

			// preparing values to be saved as tile structure
			if(key == "ID"){
				tid = string2int(value);
			}else if(key == "PATH"){
				tpath = value;
			}else if(key == "X"){
				tx = string2int(value);
				//std::cout << key << ":" << tx << ":" << value << std::endl;
			}else if(key == "Y"){
				ty = string2int(value);
				//std::cout << key << ":" << ty << ":" << value << std::endl;
			}else{
				globals_.insert(std::pair<std::string,int>(key, string2int(value)));
			}
		}
	}
	// last tile too
	t.id = tid;
	t.path = tpath;
	t.x = tx;
	t.y = ty;
	tiles_.push_back(t);

	config_file.close();
}

screener::config::~config(){
	tiles_.clear();
}


const screener::tile screener::config::operator[](const int &i){
	return tiles_[i];
}

std::size_t screener::config::size(){
	return tiles_.size();
}

bool screener::config::getGlobal(const std::string &key, int &property){
	std::map<std::string,int>::iterator it;
	it = globals_.find(key);
	if(it != globals_.end()){
		property = (*it).second;		
		return true;	
	}else{
		return false;	
	}
}

// Check for [title] subs
bool screener::config::checkSub(const std::string &line){
	std::size_t found;
	found = line.find("[");
	if(found == line.npos){
		return false;
	}else{
		return true;
	}
}

// Make better thats bloat
int screener::config::string2int(const std::string &str){
	int x=0;
	x = std::atoi(str.c_str());
	return x;
}
