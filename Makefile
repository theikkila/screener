#
# Makefile for 'main'.
#
# Type 'make' or 'make main' to create the executable.
# Type 'make clean' to delete all temporaries.
#

CC = g++

# build target specs
CFLAGS = -g -Wall -DDEBUG 
OUT_DIR = build
LIBS = -framework sfml-system -framework sfml-network -framework sfml-window -framework sfml-graphics

# first target entry is the target invoked when typing 'make'
default: main

main: $(OUT_DIR)/controller.cpp.o $(OUT_DIR)/grid.cpp.o $(OUT_DIR)/main.cpp.o $(OUT_DIR)/zoomassist.cpp.o $(OUT_DIR)/config.cpp.o
	$(CC) $(CFLAGS) -o main $(OUT_DIR)/controller.cpp.o $(OUT_DIR)/grid.cpp.o $(OUT_DIR)/main.cpp.o $(OUT_DIR)/zoomassist.cpp.o $(OUT_DIR)/config.cpp.o $(LIBS)

$(OUT_DIR)/controller.cpp.o: controller.cpp controller.h
	$(CC) $(CFLAGS) -o $(OUT_DIR)/controller.cpp.o -c controller.cpp

$(OUT_DIR)/grid.cpp.o: grid.cpp grid.h
	$(CC) $(CFLAGS) -o $(OUT_DIR)/grid.cpp.o -c grid.cpp

$(OUT_DIR)/main.cpp.o: main.cpp grid.h controller.h zoomassist.h
	$(CC) $(CFLAGS) -o $(OUT_DIR)/main.cpp.o -c main.cpp

$(OUT_DIR)/zoomassist.cpp.o: zoomassist.cpp zoomassist.h
	$(CC) $(CFLAGS) -o $(OUT_DIR)/zoomassist.cpp.o -c zoomassist.cpp

$(OUT_DIR)/config.cpp.o: config.cpp config.h
	$(CC) $(CFLAGS) -o $(OUT_DIR)/config.cpp.o -c config.cpp

clean:
	rm -f main $(OUT_DIR)/*.o