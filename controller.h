/*
* CONTROLLER.H
* Headers of network controller server
* Author: Teemu Heikkilä <teemu.heikkila@pistoke.org>
*
*/

#include <SFML/Network.hpp>
#include <vector>
#include <iostream>
#include <cstring>

namespace networking{

struct message{
	float x;
	float y;
};

class server{
public:
	server(const short unsigned int &port_i);
	~server();
	void update();
	bool getCommand(message &msg);
	bool hasMessages();
private:
	sf::SocketUDP socket;
	std::vector<message> quey_;
	short unsigned int port_;
};
}
