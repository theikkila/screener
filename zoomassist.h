#include <SFML/Graphics.hpp>

namespace screener{
class zoomassist{
	public:
		zoomassist(sf::View *Cam);
		void setZoom(const float &level);
		void iZoom(const float &level);
		float getZoom();
	private:
		float zoom_;
		sf::View *ptr_;
		sf::Vector2f hs_;
};
}
