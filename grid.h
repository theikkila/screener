// grid.h
#include <vector>
#include <SFML/Graphics.hpp>
#include <iostream>


namespace screener{
struct point{
	float x;
	float y;
};
	
class grid{
	public:
		grid(const float &x, const float &y);
		void addCell(const float &x, const float &y, const float &multiplier, sf::Sprite &spriteptr);
		int size();
		void setPos(const float &x, const float &y);
		float getX(const int &i);
		float getY(const int &i);
		sf::Sprite *getOP(const int &i);
		std::vector<sf::Sprite*> spritepointers;
	private:
		point loc_;
		int size_;
		std::vector<point> coordinates_;
		std::vector<float> multipliers_;
};
}
