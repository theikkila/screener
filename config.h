/*
* CONFIG.H
* Headers of config parser
* Author: Teemu Heikkilä <teemu.heikkila@pistoke.org>
*
*/

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <cstdlib>

namespace screener{

struct tile{
	int id;
	std::string path;
	int x;
	int y;
};

class config{
public:
	config(const std::string &path);
	~config();
	const tile operator[](const int &i);
	std::size_t size();
	bool getGlobal(const std::string &key, int &property);
private:
	bool checkSub(const std::string &line);
	int string2int(const std::string &str);
	std::map<std::string,int> globals_;
	std::vector<tile> tiles_;
};
}
