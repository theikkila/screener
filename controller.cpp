/*
* CONTROLLER.CPP
* Implementation of network controller server
* Author: Teemu Heikkilä <teemu.heikkila@pistoke.org>
*
*/

#include "controller.h"

networking::server::server(const short unsigned int &port_i){
	socket.SetBlocking(false);
	if(!socket.Bind(port_i)){
		std::cout << "[CONTROLLER-ERROR] Problem when binding port " << port_i << std::endl;
	}
	port_ = port_i;
}

networking::server::~server(){
	socket.Unbind();
	socket.Close();
}

void networking::server::update(){
	char buffer[100];
	std::size_t rcvd;
	sf::IPAddress sender;
	if(socket.Receive(buffer, sizeof(buffer), rcvd, sender, port_) != sf::Socket::Done){
		return;
	}else{
	//std::cout << "[CONTROLLER] Receiving!" << std::endl;
	networking::message msg;
	msg.x = 0;
	msg.y = 0;
	char x[sizeof(float)];
	char y[sizeof(float)];
	// Copying floats from buffer to other buffers
	std::memcpy(x, buffer, sizeof(float));
	std::memcpy(y, &(buffer[sizeof(float)]), sizeof(float));
	
	// Ugly cast
	msg.x = *(float*)x;
	msg.y = *(float*)y;
	quey_.push_back(msg);
	}
}

bool networking::server::getCommand(networking::message &msg){
	if((int)quey_.size() > 0){
		msg = quey_.back();
		quey_.pop_back();
		return true;
	}else{
		return false;
	}
}

bool networking::server::hasMessages(){
	return quey_.empty();
}
