#!/bin/bash
OS=`uname|tr '[:lower:]' '[:upper:]'`

if [[ "$OS" == "LINUX" ]]; then
echo "It seems that you have linux!"
echo "Compiling screener 1:"
g++ -O3 -Wall -Wextra -pedantic -lsfml-system -lsfml-network -lsfml-window -lsfml-graphics grid.cpp controller.cpp zoomassist.cpp config.cpp main.cpp -o screener
#echo "Compiling screener 2:"
#g++ -O3 -g -O -Wall -Weffc++ -pedantic -Wextra  -Wall -Wcast-align -Wcast-qual -Wchar-subscripts  -Wcomment -Wconversion -Wdisabled-optimization -Werror -Wfloat-equal  -Wformat  -Wformat=2 -Wformat-nonliteral -Wformat-security -Wformat-y2k -Wimplicit  -Wimport  -Winit-self  -Winline -Winvalid-pch -Wunsafe-loop-optimizations  -Wlong-long -Wmissing-braces -Wmissing-format-attribute -Wmissing-include-dirs -Wmissing-noreturn -Wpacked -Wparentheses  -Wpointer-arith -Wredundant-decls -Wreturn-type -Wsequence-point  -Wshadow -Wsign-compare  -Wstack-protector -Wstrict-aliasing -Wstrict-aliasing=2 -Wswitch  -Wswitch-default -Wswitch-enum -Wtrigraphs -Wunknown-pragmas -Wunused -Wunused-function  -Wunused-label  -Wunused-parameter -Wunused-value  -Wunused-variable  -Wvariadic-macros  -Wwrite-strings -lsfml-system -lsfml-network -lsfml-window -lsfml-graphics grid.cpp controller.cpp zoomassist.cpp config.cpp main.cpp -o screener
echo "Compiling nettest"
g++ -Wall -Wextra -lsfml-system -lsfml-network -lsfml-window -lsfml-graphics controller.cpp net_test.cpp -o net_test
echo "Compiling config_test"
g++ -Wall -Wextra config.cpp config_test.cpp -o config_test
fi
if [[ "$OS" == "DARWIN" ]]; then
echo "It seems that you have mac!"
echo "Compiling screener 1:"
g++ -O3 -Wall -Wextra -pedantic -framework sfml-system -framework sfml-network -framework sfml-window -framework sfml-graphics grid.cpp controller.cpp zoomassist.cpp config.cpp main.cpp -o screener
#echo "Compiling screener 2:"
#g++ -O3 -g -O -Wall -Weffc++ -pedantic -Wextra  -Wall -Wcast-align -Wcast-qual -Wchar-subscripts  -Wcomment -Wconversion -Wdisabled-optimization -Werror -Wfloat-equal  -Wformat  -Wformat=2 -Wformat-nonliteral -Wformat-security -Wformat-y2k -Wimplicit  -Wimport  -Winit-self  -Winline -Winvalid-pch -Wunsafe-framework oop-optimizations  -Wlong-framework ong -Wmissing-braces -Wmissing-format-attribute -Wmissing-include-dirs -Wmissing-noreturn -Wpacked -Wparentheses  -Wpointer-arith -Wredundant-decls -Wreturn-type -Wsequence-point  -Wshadow -Wsign-compare  -Wstack-protector -Wstrict-aliasing -Wstrict-aliasing=2 -Wswitch  -Wswitch-default -Wswitch-enum -Wtrigraphs -Wunknown-pragmas -Wunused -Wunused-function  -Wunused-framework abel  -Wunused-parameter -Wunused-value  -Wunused-variable  -Wvariadic-macros  -Wwrite-strings -framework sfml-system -framework sfml-network -framework sfml-window -framework sfml-graphics grid.cpp controller.cpp zoomassist.cpp config.cpp main.cpp -o screener
echo "Compiling nettest"
g++ -Wall -Wextra -framework sfml-system -framework sfml-network -framework sfml-window -framework sfml-graphics controller.cpp net_test.cpp -o net_test
echo "Compiling config_test"
g++ -Wall -Wextra config.cpp config_test.cpp -o config_test
fi
