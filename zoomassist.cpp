#include "zoomassist.h"

screener::zoomassist::zoomassist(sf::View *Cam): zoom_(1), ptr_(Cam), hs_((ptr_)->GetHalfSize()){
	zoom_ = 1;
	ptr_ = Cam;
	hs_ = (ptr_)->GetHalfSize();
	//std::cout << "(" << _hs.x << "," << _hs.y << ")" << std::endl;
}
void screener::zoomassist::setZoom(const float &level){
	zoom_ = level;
	(ptr_)->SetHalfSize(hs_.x*zoom_, hs_.y*zoom_);
	//std::cout << "(" << _hs.x*_zoom << "," << _hs.y*_zoom << ")   LEVEL: " << _zoom <<  std::endl;
}

void screener::zoomassist::iZoom(const float &level){
	zoom_ *= level;
}

float screener::zoomassist::getZoom(){
	return zoom_;
}
