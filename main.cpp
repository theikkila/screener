/*
* MAIN.CPP
* SCREENER, the ultimate app for nice presentations
*
* Author: Teemu Heikkilä <teemu.heikkila@pistoke.org>
* Version: 0.1
*/

#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include "grid.h"
#include "controller.h"
#include "config.h"
#include "zoomassist.h"

#define PI 3.141592


namespace interpolating{
	float cospolate(const float &t, const float &s, const float &e){
		float mu2, y;
		mu2 = (1.f-(float)std::cos(t*0.4f*PI));
		y = (s*(1.f-mu2)+e*mu2);
		return y;
	}

	float chs(const float &t, const float &s, const float &e){
		float y=0.f;
		const float m0 = 1.1f;
		const float m1 = 1.1f;
		y = (2.f*t*t*t-3.f*t*t+1.f)*s+(t*t*t-2.f*t*t+t)*m0+(-2.f*t*t*t+3.f*t*t)*e+(t*t*t-t*t)*m1;
		return y;
	}
}
int main()
{
	screener::config cfg("tiles.cfg");
	static int PORT = 14000;
	static int R_SPEED = 10;
	static int INTERPOLATION_SPEED = 10;
	static int ZOOM_SPEED = 10;
	static int ZOOM = 14;
	cfg.getGlobal("PORT", PORT);
	cfg.getGlobal("R_SPEED", R_SPEED);
	cfg.getGlobal("ZOOM", ZOOM);
	cfg.getGlobal("INTERPOLATION_SPEED", INTERPOLATION_SPEED);
	cfg.getGlobal("ZOOM_SPEED", ZOOM_SPEED);

	sf::RenderWindow App(sf::VideoMode::GetMode(0), "SCREENER v0.1", sf::Style::Fullscreen);
	networking::server cserver(PORT);	
	// Initialising variables
	bool Running = true;
	sf::Clock iclock;
	bool interpolate = false, manual=false;
	int phase=0; // phase
	// Current X,Y, Point to go X,Y, Interpolated X,Y
	static screener::point current, togo, ipolated;
	float mx=0, dx=0, my=0, dy=0; // Manual X,Y, Delta X,Y
	sf::Image bg, part1, part2, part3;
	sf::Sprite bgs, ps1, ps2, ps3;
	
	// Load main elements
	if(!bg.LoadFromFile("bg.jpg")){
		std::cout << "Failed to load background" << std::endl;
	}
	if(!part1.LoadFromFile("part1.png")){
		std::cout << "Failed to load part1" << std::endl;
	}
	if(!part2.LoadFromFile("part2.png")){
		std::cout << "Failed to load part2" << std::endl;
	}
	if(!part3.LoadFromFile("part3.png")){
		std::cout << "Failed to load part3" << std::endl;
	}
	
	// Properties for main elements
	bgs.SetImage(bg);
	ps1.SetImage(part1);
	ps2.SetImage(part2);
	ps3.SetImage(part3);
	
	bgs.SetCenter(bgs.GetSize().x/2, bgs.GetSize().y/2);
	ps2.SetCenter(ps2.GetSize().x/2, ps2.GetSize().y/2);
		
	bgs.SetScale(3.f,3.f);
	ps1.SetScale(0.6f,0.6f);
	ps2.SetScale(1.f,1.f);
	ps3.SetScale(4.f,4.f);
	
	ps1.SetPosition(200.f,100.f);
	ps3.SetPosition(0.f,0.f);

	// Initializing mgrid and setting main elements there
	screener::grid mgrid(0,0);
	mgrid.addCell(((float)App.GetWidth()/2.f)+500.f, ((float)App.GetHeight()/2.f)+1000.f, -0.90f, bgs);
	mgrid.addCell(0.f, 0.f, -0.65f, ps1);
	mgrid.addCell(((float)App.GetWidth()/2.f)+500.f, ((float)App.GetHeight()/2.f)+1000.f, -0.5f, ps2);
	
	// Initializing vectors for tiles
	std::vector<sf::Image> tile_images;
	std::vector<sf::Sprite> tile_sprites;
	tile_images.resize(cfg.size());
	tile_sprites.resize(cfg.size());
	
	// Load and setup for tiles
	for(unsigned int i=0; i<cfg.size(); i++){
		if(tile_images[i].LoadFromFile(cfg[i].path)){
			std::cout << "[LOADED] " << cfg[i].path << std::endl;
			tile_sprites[i].SetImage(tile_images[i]);
			tile_sprites[i].SetPosition(0.f, 0.f);
			tile_sprites[i].SetScale(1.f, 1.f);
			mgrid.addCell((float)cfg[i].x, (float)cfg[i].y, 0.f, tile_sprites[i]);
		}else{
			std::cout << "[FAILED] Unable to load tile: " << cfg[i].id << " @ " << cfg[i].path << std::endl;
		}
	}
	
	// Setting up camera
	sf::Vector2f Center(0,0);
	sf::Vector2f HalfSize(720, 540);
	sf::View Cam(Center, HalfSize);
	App.SetView(Cam);
	// zoomassistant
	Cam.Zoom((float)ZOOM/10.f);
	screener::zoomassist as1(&Cam);
	Cam.SetCenter(0, 0);

	//Starting position of mgrid
	mgrid.setPos(0, 0);
	
	App.Clear();
	App.Draw(bgs);
	App.Display();
	App.SetFramerateLimit(30);
	while (Running){
	    sf::Event Event;
	    while (App.GetEvent(Event))
	    {
	        if(Event.Type == sf::Event::Closed){
				Running = false;
			}
			if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::Escape)){
			 	Running = false;
			}
			
			if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::Z)){
				if(interpolate){
					current.x = ipolated.x;
					current.y = ipolated.y;
					togo.x = 0;
					togo.y = 0;
					iclock.Reset();
				}else if(!interpolate){
					togo.x = 0;
					togo.y = 0;
					iclock.Reset();
					interpolate = true;
				}
			}
			if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::X)){
				if(interpolate){
					current.x = ipolated.x;
					current.y = ipolated.y;
					togo.x = 3000;
					togo.y = 0;
					iclock.Reset();
				}else if(!interpolate){
					togo.x = 3000;
					togo.y = 0;
					iclock.Reset();
					interpolate = true;
				}
			}
			if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::C)){
				if(interpolate){
					current.x = ipolated.x;
					current.y = ipolated.y;
					togo.x = 400;
					togo.y = -800;
					iclock.Reset();
				}else if(!interpolate){
					togo.x = 400;
					togo.y = -800;
					iclock.Reset();
					interpolate = true;
				}
			}
			
			if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::V)){
				if(interpolate){
					current.x = ipolated.x;
					current.y = ipolated.y;
					togo.x = 3000;
					togo.y = -800;
					iclock.Reset();
				}else if(!interpolate){
					togo.x = 3000;
					togo.y = -800;
					iclock.Reset();
					interpolate = true;
				}
			}

			if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::R)){
				as1.setZoom(1);
				interpolate = false;
				App.Clear();
			}
			if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::I)){
				/*Cam.Zoom(1.1);
				as1.iZoom(1.1);*/
					as1.setZoom(0.5);
			}
			if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::O)){
				/*Cam.Zoom(0.9);
				as1.iZoom(0.9);*/
					as1.setZoom(5);
	    	}
		}
		/*
		* REMOTE COMMANDS
		*/
		
		// Fetch new commands from client
		cserver.update();
		networking::message next;
		if(cserver.getCommand(next)){
			interpolate = true;
			togo.x = next.x;
			togo.y = next.y;
			iclock.Reset();
			std::cout << "[COMMAND] from udp: " << togo.x << "," << togo.y << std::endl;
		}
		
		/*
		* INTERPOLATING 
		*/
		
		// If don't interpolate then reset clock and set current position to be the starting position for next transition
		if(!interpolate){
			iclock.Reset();
			current.x = togo.x;
			current.y = togo.y;
			
		}
		/*
		* When interpolating, then go from phase 0->2
		* 0 : Zoom out
		* 1 : Move camera
		* 2 : Zoom in
		*/
		// Phase 0: Zoom out
		if(interpolate && phase==0){
			as1.setZoom(interpolating::chs(iclock.GetElapsedTime()*2.0*(10.0/ZOOM_SPEED), 7, 10)/10);
			
			if(as1.getZoom() >= 1){
				phase++;
				iclock.Reset();
			}
		}
		// Phase 1: Move camera
		if(interpolate && phase==1){
			// Interpolate
			ipolated.x = interpolating::chs(iclock.GetElapsedTime()/(float)INTERPOLATION_SPEED, current.x, togo.x);
			ipolated.y = interpolating::chs(iclock.GetElapsedTime()/(float)INTERPOLATION_SPEED, current.y, togo.y);
			// If manual (deprecated)
			if(manual){
				Cam.SetCenter(mx+dx, my+dy);
				mgrid.setPos(mx+dx, my+dy);
				mx = mx + dx;
				my = my + dy;
				dy = 0;
				dx = 0;
				std::cout << mx << "," << my << std::endl;
				interpolate = false;
				manual=false;
			}else{
				// Moving camera and mgrid to the interpolated position.
				mgrid.setPos(ipolated.x, ipolated.y);
				Cam.SetCenter(ipolated.x, ipolated.y);
				
				// Set the position of slides by iterating trough mgrid
				for(int i=0; i<mgrid.size();i++){
					(mgrid.spritepointers[i])->SetPosition(mgrid.getX(i), mgrid.getY(i));
				}
				if(std::abs(togo.x-ipolated.x) < 1 && std::abs(togo.y-ipolated.y) < 1){
					//interpolate = false;
					phase++;
					iclock.Reset();
				}
			}
		}

		// Phase 2: Zoom in
		if(interpolate && phase==2){
			as1.setZoom(interpolating::chs(iclock.GetElapsedTime()*2.f*(10.f/(float)ZOOM_SPEED), 10, 7)/10);
			
			if(as1.getZoom() <= 0.7){
				phase=0;
				interpolate=false;
				iclock.Reset();
			}
		}
		// Rotate bubbles in background
		ps2.Rotate((float)R_SPEED/100.f); //0.003;
		
		App.Clear();
		App.Draw(bgs);
		App.Draw(ps1);
		App.Draw(ps2);
		// Draw all tiles
		for(unsigned int i=0; i<tile_sprites.size(); i++){
			App.Draw(tile_sprites[i]);
		}
		App.Draw(ps3);
		//App.Draw(demo_text);
	    App.Display();
	}
    return EXIT_SUCCESS;
}


