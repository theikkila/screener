#include "grid.h"

screener::grid::grid(const float &x, const float &y){
	size_ = 0;
	loc_.x = x;
	loc_.y = y;
}

void screener::grid::addCell(const float &x, const float &y, const float &multiplier, sf::Sprite &spriteptr){
	++size_;
	point tmpp;
	tmpp.x = x - spriteptr.GetSize().x/2;
	tmpp.y = y - spriteptr.GetSize().y/2;
	spritepointers.push_back(&spriteptr);
	coordinates_.push_back(tmpp);
	multipliers_.push_back(multiplier);
}

int screener::grid::size(){
	return size_;
}

void screener::grid::setPos(const float &x, const float &y){
	float dx, dy;
	dx = loc_.x-x;
	dy = loc_.y-y;
	loc_.x = x;
	loc_.y = y;
	for(int i=0; i<size_;i++){
		
		coordinates_[i].x = coordinates_[i].x + multipliers_[i]*dx;//;
		coordinates_[i].y = coordinates_[i].y + multipliers_[i]*dy;// + (((ptrs[i])->GetSize().y)/2);
		
		/*
		coordinates[i].x = coordinates[i].x + dx;
		coordinates[i].y = coordinates[i].y + dy;
		*/
	}
}

float screener::grid::getX(const int &i){
	return coordinates_[i].x;
}

float screener::grid::getY(const int &i){
	return coordinates_[i].y;
}

sf::Sprite* screener::grid::getOP(const int &i){
	return spritepointers[i];
}
