/*
* CONFIG_TEST.CPP
* Test for config parser
* Author: Teemu Heikkilä <teemu.heikkila@pistoke.org>
*
*/

#include "config.h"
#include <iostream>

int main(){
	screener::config cfg("tiles.cfg");
	int port, r_speed;
	std::cout << "Tiles: " << cfg.size() << std::endl;
	std::cout << "GLOBALS:" << std::endl;
	if(cfg.getGlobal("PORT", port)){ std::cout << "Port: " << port << std::endl; }
	if(cfg.getGlobal("R_SPEED", r_speed)){ std::cout << "Rotating speed: " << r_speed << std::endl; } 
	for(int i=0; i<(int)cfg.size(); i++){	
		std::cout << "ID: " << cfg[i].id << "  path: " << cfg[i].path <<"  X: " << cfg[i].x << " Y: " << cfg[i].y << std::endl;
	}
	return 0;
}
